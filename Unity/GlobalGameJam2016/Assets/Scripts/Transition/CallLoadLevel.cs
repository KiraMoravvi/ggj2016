﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

public class CallLoadLevel : MonoBehaviour
{
    public void LoadLevel(string levelName)
    {
        Application.LoadLevel(levelName);
    }
}