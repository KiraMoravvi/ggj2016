﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

public class OpenTwitter : MonoBehaviour
{
    public void OpenURL(string twitterTag)
    {
        Application.OpenURL("https://twitter.com/" + twitterTag);
    }
}