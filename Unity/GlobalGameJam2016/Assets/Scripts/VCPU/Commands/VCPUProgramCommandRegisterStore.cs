﻿using System.Collections;

public class VCPUProgramCommandRegisterStore : VCPUProgramCommand {
    public VCPUProgramCommandRegisterStore() {
        _name = "Register Store";
        _description = "Stores a register to a location in memory (See register description to see what each bot uses the register for)";
        _instructions[0] = (uint)VCPU.INSTRUCTIONS.REGISTER_STORE;
        _length = 2;
    }

    public VCPUProgramCommandRegisterStore(uint register, uint memAddress) {
        _name = "Register Store";
        _description = "Stores a register to a location in memory (See register description to see what each bot uses the register for)";

        _instructions[0] = (uint)VCPU.INSTRUCTIONS.REGISTER_STORE;

        SetTargetMemAddress(memAddress);
        SetTargetRegister(register);

        _length = 2;
    }

    public void SetTargetRegister(uint register) {
        _instructions[0] = (register << 16) | _instructions[0];
    }

    public uint GetTargetRegister() {
        return _instructions[0] >> 16;
    }

    public void SetTargetMemAddress(uint memAddress) {
        _instructions[1] = memAddress;
    }

    public uint GetTargetMemAddress() {
        return _instructions[1];
    }
}
