﻿using System.Collections;

public class VCPUProgramCommandRegisterLoad : VCPUProgramCommand {
    bool _ptr = true;

    public VCPUProgramCommandRegisterLoad() {
        _name = "Register Load";
        _description = "Loads a value from location in memory and stores it in a register (See register description to see what each bot uses the register for)";
        _instructions[0] = (uint)VCPU.INSTRUCTIONS.REGISTER_LOAD;
        _length = 2;
    }

    public VCPUProgramCommandRegisterLoad(uint register, uint memAddress, bool pointer = true) {
        _name = "Register Load";
        _description = "Loads a value from location in memory and stores it in a register (See register description to see what each bot uses the register for)";
        _instructions[0] = (uint)VCPU.INSTRUCTIONS.REGISTER_LOAD;

        if(pointer == false) {
            _instructions[0] = (uint)VCPU.INSTRUCTIONS.REGISTER_SET;
        }

        SetTargetMemAddress(memAddress);
        SetTargetRegister(register);

        _length = 2;
    }

    public void SetTargetRegister(uint register) {
        _instructions[0] = (register << 16) | _instructions[0];
    }

    public uint GetTargetRegister() {
        return _instructions[0] >> 16;
    }

    public void SetTargetMemAddress(uint memAddress) {
        _instructions[1] = memAddress;
    }

    public uint GetTargetMemAddress() {
        return _instructions[1];
    }
}
