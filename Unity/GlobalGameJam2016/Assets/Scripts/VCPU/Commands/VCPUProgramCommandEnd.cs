﻿using UnityEngine;
using System.Collections;

public class VCPUProgramCommandEnd : VCPUProgramCommand {
    public VCPUProgramCommandEnd() {
        _name = "End Program";
        _instructions[0] = (uint)VCPU.INSTRUCTIONS.STOP;
    }
}
