﻿using System.Collections;
using System.Collections.Generic;

public class VCPUProgram {
    List<VCPUProgramCommand> _commands;

    public VCPUProgram() {
        _commands = new List<VCPUProgramCommand>();
    }

    public void Clear() {
        this._commands.Clear();
    }

    public void Set(int index, VCPUProgramCommand command) {
        _commands[index] = command;
    }

    public VCPUProgramCommand Get(int index) {
        return _commands[index];
    }

    public void Add(VCPUProgramCommand command) {
        _commands.Add(command);
    }

    public int Length() {
        return _commands.Count;
    }
}
