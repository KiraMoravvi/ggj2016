﻿using System.Collections;
using System.Collections.Generic;
using System;

public class VCPUProgramCommand : ICloneable  {
    protected uint[] _instructions = new uint[4];
    protected byte _length;
    protected string _description;
    protected string _name;

    protected VCPUProgramCommand() {

    }

    public VCPUProgramCommand(uint instruction, uint[] parameters = null) {
        _instructions[0] = instruction;
        _length = 1;
        int paramLength = 0;

        if(parameters != null) {
            paramLength = parameters.Length;
            if(paramLength > 3) {
                paramLength = 3;
            }
        }

        _length += (byte)paramLength;

        for(int i = 0;i < paramLength;++i) {
            _instructions[i + 1] = parameters[i];
        }
    }

    public virtual string GetName() {
        return _name;
    }

    public virtual string GetDescription() {
        return _description;
    }

    public uint[] GetInstructions() {
        return _instructions;
    }

    public byte GetLength() {
        return _length;
    }

    public Object Clone() {
        uint[] parameters = new uint[3];
        for(int i = 1; i < _length - 1;++i) {
            parameters[i - 1] = _instructions[i];
        }

        VCPUProgramCommand cloned = new VCPUProgramCommand(_instructions[0],parameters);
        cloned._name = _name;
        cloned._description = _description;
        cloned._length = _length;

        return cloned;
    }
}
