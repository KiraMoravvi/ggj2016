﻿using System.Collections;
using System.Collections.Generic;

public class VCPU {


    public enum INSTRUCTIONS : ushort {
        //GENERIC CPU OPERATIONS
        NOP = 0,                    //DO NOTHING
        STOP = 1,                   //STOP THE PROGRAM
        REGISTER_LOAD = 2,          //LOAD VALUE AT REGISTER
        REGISTER_SET  = 3,          //SET THE VALUE AT THE REGISTER TO A SPECIFIC VALUE
        REGISTER_STORE = 4,         //STORE VALUE AT REGISTER
        JUMP = 5,                   //JUMP TO ANOTHER INSTRUCTION
        INC = 6,
        DEC = 7,

        //MATH OPERATIONS
        ADD = 10,                   //ADD A VALUE
        SUB = 11,                   //SUB A VALUE
        MULT = 12,                  //MULTIPLY A VALUE
        DIV = 13,                   //DIVIDE A VALUE
        MOD = 14,                   //MODULUS OF A VALUE
    }

    protected bool _overflow;
    protected bool _running;
    protected bool _error;
    protected bool _halt;
    protected bool _loaded;
    
    protected uint _instructionCursor;

    protected uint[] _program;

    protected uint[] _registers;
    protected float[] _floatRegisters;

    public VCPU() {
        _program = new uint[65536];
        _registers = new uint[16];
        _floatRegisters = new float[16];
        this.Reset();
        this.Unload();
    }

    public void Boot() {
        if(_loaded) {
            if(!_running) {
                _instructionCursor = 0;
                _running = true;
            }
        }
    }

    public bool IsHalted() {
        return _halt;
    }

    public void Halt() {
        _halt = true;
    }

    public void Resume() {
        if(_running && _halt == true) {
            _halt = false;
        }
    }

    public void Step(uint cycles) {
        uint[] tmp = new uint[8];

        while(cycles > 0) {
            --cycles;
            if(_running && !_halt) {
                //GET THE CURRENT INSTRUCTION IN THE PROGRAM

                uint curInstruction = _program[_instructionCursor];

                ushort instruction = (ushort)(curInstruction & 0x0000FFFF);       //USE A BITMASK TO GET THE LOWER 16 BITS
                ushort option = (ushort)(curInstruction >> 16);               //SHIFT 16 BITS TO THE LEFT TO GET THE LOWER 16 BITS

                byte optionLo = (byte)(option & 0x00FF);
                byte optionHi = (byte)(option >> 8);

                switch(instruction) {
                    case (ushort)INSTRUCTIONS.NOP:
                        break;
                    case (ushort)INSTRUCTIONS.STOP:
                        _halt = true;
                        break;
                    case (ushort)INSTRUCTIONS.REGISTER_LOAD:
                        ++_instructionCursor;
                        _registers[option] = _program[_program[_instructionCursor]];
                        break;
                    case (ushort)INSTRUCTIONS.REGISTER_SET:
                        ++_instructionCursor;
                        _registers[option] = _program[_instructionCursor];
                        break;
                    case (ushort)INSTRUCTIONS.REGISTER_STORE:
                        //STORES THE VALUE IN REGISTER IN OPTION AT MEMORY ADDRESS SPECIFIED AT INSTRUCTIONCURSOR
                        ++_instructionCursor;
                        _program[_program[_instructionCursor]] = _registers[option];
                        break;
                    case (ushort)INSTRUCTIONS.JUMP:
                        //STORES THE VALUE IN REGISTER IN OPTION AT MEMORY ADDRESS SPECIFIED AT INSTRUCTIONCURSOR
                        ++_instructionCursor;
                        _instructionCursor = _program[_instructionCursor];
                        continue;         //DONT JUMP TO THE NEXT THING
                    case (ushort)INSTRUCTIONS.INC:
                        _registers[option]++;
                        break;
                    case (ushort)INSTRUCTIONS.DEC:
                        _registers[option]--;
                        break;
                    case (ushort)INSTRUCTIONS.ADD: {
                            ++_instructionCursor;

                            //0 = READ FROM INT REGISTER
                            //1 = READ FROM FLOAT REGISTER
                            //2 = READ FROM MEMORY AT POSITION

                            switch(optionHi) {
                                case 0:
                                    tmp[0] = _registers[_program[_instructionCursor]];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    tmp[0] = _program[_program[_instructionCursor]];
                                    break;
                                case 3:
                                    //USE FIXED VALUE
                                    tmp[0] = _program[_instructionCursor];
                                    break;
                            }

                            ++this._instructionCursor;

                            switch(optionLo) {
                                case 0:
                                    _registers[_program[_instructionCursor]] += tmp[0];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    _program[_instructionCursor] += tmp[0];
                                    break;
                            }
                        } break;
                    case (ushort)INSTRUCTIONS.SUB: {
                            ++_instructionCursor;

                            //0 = READ FROM INT REGISTER
                            //1 = READ FROM FLOAT REGISTER
                            //2 = READ FROM MEMORY AT POSITION

                            switch(optionHi) {
                                case 0:
                                    tmp[0] = _registers[_program[_instructionCursor]];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    tmp[0] = _program[_program[_instructionCursor]];
                                    break;
                                case 3:
                                    //USE FIXED VALUE
                                    tmp[0] = _program[_instructionCursor];
                                    break;
                            }

                            ++this._instructionCursor;

                            switch(optionLo) {
                                case 0:
                                    _registers[_program[_instructionCursor]] -= tmp[0];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    _program[_instructionCursor] -= tmp[0];
                                    break;
                            }
                        }
                        break;
                    case (ushort)INSTRUCTIONS.MULT: {
                            ++_instructionCursor;

                            //0 = READ FROM INT REGISTER
                            //1 = READ FROM FLOAT REGISTER
                            //2 = READ FROM MEMORY AT POSITION

                            switch(optionHi) {
                                case 0:
                                    tmp[0] = _registers[_program[_instructionCursor]];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    tmp[0] = _program[_program[_instructionCursor]];
                                    break;
                                case 3:
                                    //USE FIXED VALUE
                                    tmp[0] = _program[_instructionCursor];
                                    break;
                            }

                            ++_instructionCursor;

                            switch(optionLo) {
                                case 0:
                                    _registers[_program[_instructionCursor]] *= tmp[0];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    _program[_instructionCursor] *= tmp[0];
                                    break;
                            }
                        }
                        break;
                    case (ushort)INSTRUCTIONS.DIV: {
                            ++_instructionCursor;

                            //0 = READ FROM INT REGISTER
                            //1 = READ FROM FLOAT REGISTER
                            //2 = READ FROM MEMORY AT POSITION

                            switch(optionHi) {
                                case 0:
                                    tmp[0] = _registers[_program[_instructionCursor]];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    tmp[0] = _program[_program[_instructionCursor]];
                                    break;
                                case 3:
                                    //USE FIXED VALUE
                                    tmp[0] = _program[_instructionCursor];
                                    break;
                            }

                            ++_instructionCursor;

                            switch(optionLo) {
                                case 0:
                                    _registers[_program[_instructionCursor]] /= tmp[0];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    _program[_instructionCursor] /= tmp[0];
                                    break;
                            }
                        }
                        break;
                    case (ushort)INSTRUCTIONS.MOD: {
                            ++_instructionCursor;

                            //0 = READ FROM INT REGISTER
                            //1 = READ FROM FLOAT REGISTER
                            //2 = READ FROM MEMORY AT POSITION

                            switch(optionHi) {
                                case 0:
                                    tmp[0] = _registers[_program[_instructionCursor]];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    tmp[0] = _program[_program[_instructionCursor]];
                                    break;
                                case 3:
                                    //USE FIXED VALUE
                                    tmp[0] = _program[_instructionCursor];
                                    break;
                            }

                            ++_instructionCursor;

                            switch(optionLo) {
                                case 0:
                                    _registers[_program[_instructionCursor]] %= tmp[0];
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    _program[_instructionCursor] %= tmp[0];
                                    break;
                            }
                        }
                        break;
                }

                ++_instructionCursor;
            }
        }
    }

    public uint GetRegister(uint register) {
        return _registers[register];
    }

    public float GetRegisterFloat(uint register) {
        return _floatRegisters[register];
    }

    public void Unload() {
        //CLEAR PROGRAM
        for(uint i = 0; i < this._program.Length; i++) {
            _program[i] = 0;
        }

        _loaded = false;
    }

    /*
    * Interprets a program object and turns it into a format that the CPU can understand. Also changes jump instructions to point towards the correct program location,
    * since the jump instructions in the program will point towards a program location as opposed to a proper location
    */
    public void Load(VCPUProgram program) {
        
        //LOADS PROGRAM INTO MEMORY
        if(!_running) {
            this.Unload();
            _loaded = true;

            List<uint> commandStartPos = new List<uint>(program.Length());           //STORES THE PROGRAMS START POSITION

            Dictionary<uint, int> jumpCommands = new Dictionary<uint, int>();

            int programPosition = 0;
            int programLength = program.Length();

            uint programWriteCursor = 0;
            while(programPosition < programLength) {
                //COPY ALL OPCODES INTO MEMORY - IF YOU INPUT A JUMP TO A COMMAND NUMBER, IT WILL TRANSLATE THE JUMP TO A RELATIVE POSITION WITHIN THAT COMMAND
                
                commandStartPos.Add(programWriteCursor);
                
                uint[] curProgramOpCodes;

                //for(int curProgramPosition = 0;curProgramPosition < programLength;curProgramPosition++) {
                    VCPUProgramCommand curCommand = program.Get(programPosition);
                    curProgramOpCodes = curCommand.GetInstructions();

                    ushort instruction = (ushort)(curProgramOpCodes[0] & 0x00FF);       //USE A BITMASK TO GET THE LOWER 16 BITS
                    ushort option = (ushort)(curProgramOpCodes[0] >> 16);               //SHIFT 16 BITS TO THE LEFT TO GET THE LOWER 16 BITS
                    
                    if(
                        instruction == (ushort)INSTRUCTIONS.JUMP || 
                        instruction == (ushort)INSTRUCTIONS.REGISTER_LOAD || 
                        instruction == (ushort)INSTRUCTIONS.REGISTER_STORE
                    ) {
                        jumpCommands[programWriteCursor] = (int)curProgramOpCodes[1];
                        //CHANGE LOOP TO JUMP POSITION, WHICH WILL BE THE SECOND ITEM IN THE OPCODE
                    }

                    for(byte i = 0; i < curCommand.GetLength(); ++i) {
                        _program[programWriteCursor] = curProgramOpCodes[i];

                        ++programWriteCursor;
                    }

                //}
                 
                ++programPosition;
            }

            //ALL JUMP COMMANDS CURRENTLY POINT TO PROGRAM POSITIONS INSTEAD OF BYTE POSITIONS - NEED TO GO THROUGH AND ADJUST THIS!
            foreach(var pair in jumpCommands) {
                uint targetByte = commandStartPos[pair.Value];
                _program[pair.Key + 1] = targetByte;
            }            
        }
    }

    public void Reset() {

        _instructionCursor = 0;
        _overflow = false;
        _running = false;
        _error = false;
        _halt = false;
        for(uint i = 0;i < _registers.Length;i++) {
            _registers[i] = 0;
        }

    }
}
