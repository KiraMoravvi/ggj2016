﻿using UnityEngine;
using System.Collections;

public class TileMovement : MonoBehaviour {

    [SerializeField]
    protected MapGenerator mapGen;

    [SerializeField]
    protected int _xPos;
    [SerializeField]
    protected int _yPos;
    [SerializeField]
    protected float speed = 1.0f;

    [SerializeField]
    private Vector3 currentPos;

    [SerializeField]
    private Vector3 futurePos;
    private float time = 0.0f;

    protected Vector2 target;

    protected bool _doMove = false;

    // Use this for initialization
    void Start () {
        mapGen = FindObjectOfType<MapGenerator>();
        transform.localPosition = currentPos = futurePos = mapGen.GetPosition(_xPos, _yPos);
    }

    public bool AtTargetPos() {
        if(currentPos.x == futurePos.x && currentPos.y == futurePos.y) {
            return true;
        }

        return false;
    }

    public void SetTargetPos(int x, int y, float speed) {

        futurePos = mapGen.GetPosition(x, y);

        time = 0.0f;
        _doMove = true;
        this.speed = speed;
    }

    public void SetTargetY(int y, float speed) {

        futurePos = mapGen.GetPosition(_xPos, y);

        time = 0.0f;
        _doMove = true;
        this.speed = speed;

        _yPos = y;
    }

    public void SetTargetX(int x, float speed) {

        futurePos = mapGen.GetPosition(x, _yPos);

        time = 0.0f;
        _doMove = true;
        this.speed = speed;

        _xPos = x;
    }
	
    public Vector2 GetTargetPos() {
        return new Vector2(futurePos.x, futurePos.y);
    }
    
    
	// Update is called once per frame
	void Update ()
    {
        if(_doMove)
        {
            //time += Time.deltaTime * speed;
            Vector3 pos = transform.localPosition;

            //pos.x = Mathf.Lerp(currentPos.x, futurePos.x, time);
           // pos.y = Mathf.Lerp(currentPos.y, futurePos.y, time);

            pos = Vector3.MoveTowards(pos,futurePos,speed);

            transform.localPosition = pos;

            //if (time >= 1)
            if(Vector3.Distance(pos,futurePos) < 0.01f)
            {
                time = 0.0f;
                currentPos = futurePos;
                _doMove = false;
            }
        }
    }

    public bool IsMoving()
    {
        return _doMove;
    }
}
