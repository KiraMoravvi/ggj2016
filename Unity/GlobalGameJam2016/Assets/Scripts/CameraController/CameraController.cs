﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

public class CameraController : MonoBehaviour
{
	protected static CameraController _instance;
	
	[SerializeField]
	protected Camera _cam;
	
	protected float _maxSize = 1f;
	protected float _minSize = 3f;
	
	protected float _curSize = 1f;
	
	private int _width;
	private int _height;
	
	private float _sizeOfSprite;
	
	private Vector2 _mousePosDrag;
	
	[SerializeField]
	protected float _panSpeed;
	
	protected void Update()
	{
		Vector3 pos = new Vector3(0f, 0f, 0f);
		
		float scrollAmountY = 0f;
        if (Input.GetMouseButtonDown(2) || Input.GetMouseButtonDown(1))
		{
			_mousePosDrag = Input.mousePosition;
		}
        else if (Input.GetMouseButton(2) || Input.GetMouseButtonDown(1))
		{
			scrollAmountY = (_mousePosDrag.y - Input.mousePosition.y) * _panSpeed;
			
			if (scrollAmountY != 0f)
			{
				transform.position += new Vector3(0f,scrollAmountY, 0f);
			}
			
			pos = transform.position;
			float posY = pos.y;
			
			posY = Mathf.Clamp(posY, 0-(_height * _sizeOfSprite),0f);
			
			if (scrollAmountY != 0f)
			{
				pos.y = posY;
			}
			
			transform.position = pos;
		}
		else
		{
			scrollAmountY = Input.mouseScrollDelta.y;
			if (scrollAmountY != 0f)
			{
				_curSize -= scrollAmountY;
				if (_curSize > _maxSize)
				{
					_curSize = _maxSize;
				}
				if(_curSize < _minSize)
				{
					_curSize = _minSize;
				}
				_cam.orthographicSize = _curSize;
			}
		}
		
		float scrollAmountX = 0f;
        if (Input.GetMouseButton(2) || Input.GetMouseButtonDown(1))
		{
			scrollAmountX = (_mousePosDrag.x - Input.mousePosition.x) * _panSpeed;
		}
		else
		{
			scrollAmountX = Input.mouseScrollDelta.x;
		}
		
		pos = new Vector3(0f, 0f, 0f);
		
		
		if (scrollAmountX != 0f)
		{
			transform.position += new Vector3(scrollAmountX, 0f, 0f);
		}
		
		pos = transform.position;
		float posX = pos.x;
		
		posX = Mathf.Clamp(posX, 0f, _width * _sizeOfSprite);
		
		if (scrollAmountX != 0f)
		{
			pos.x = posX;
		}
		
		transform.position = pos;
		
        if (Input.GetMouseButton(2) || Input.GetMouseButtonDown(1))
		{
			_mousePosDrag = Input.mousePosition;
		}
	}
	
	public void PositionCamera(float sizeOfSprite, int width, int height)
	{
		_width = width;
		_height = height;
		_sizeOfSprite = sizeOfSprite;
		
		_cam = Camera.main;
		
		_maxSize = (sizeOfSprite * height) / 2f;
		_curSize = _maxSize;
		
		_cam.orthographicSize = _curSize;
		
		float posX = (sizeOfSprite * width) / 2f;
		float posY = 0f - ((sizeOfSprite * height) / 2f) + (sizeOfSprite / 2f);
		
		_cam.transform.position = new Vector3(posX, posY, -10f);
	}
	
	public static CameraController Instance()
	{
		if (_instance != null)
		{
			return _instance;
		}
		else
		{
			_instance = FindObjectOfType<CameraController>();
			return _instance;
		}
	}
}