﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CommandElement: MonoBehaviour {
    [SerializeField]
    private string _name;
    [SerializeField]
    private string _description;

    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
            foreach (Transform child in transform)
                if (child.name == "Name")
                {
                    
                    foreach (Transform child2 in child.transform)
                    {
                        child2.GetComponent<Text>().text = _name; ;
                    }
                }


        }
    }

    public string Description
    {
        get
        {
            return _description;
        }

        set
        {
            _description = value;
        }
    }    

    public CommandElement(string name,string description){
        this.Name = name;
        this.Description = description;
        }

    public void ChangeDescription()
    {
        GameObject descriptionpanel = GameObject.Find("DescriptionPanel");


        foreach (Transform child in descriptionpanel.transform) { 
            Debug.Log(child.name);
        if (child.name == "description")
            {
                Debug.Log(this.Description);
               
                child.GetComponent<Text>().text = this.Description;
            }
        }
    }
}
