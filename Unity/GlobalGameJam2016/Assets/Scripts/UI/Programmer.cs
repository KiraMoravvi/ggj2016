﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

class Programmer : MonoBehaviour {
    public Bot target;
    Dictionary<string, VCPUProgramCommand> commands;

    public GameObject prefab;
    public GameObject commandDisplayPrefab;
    public void ShowProgram() {

        VCPUProgram botProgram = target.GetProgram();

        GameObject btnParent = GameObject.Find("ProgramCommandList");

        var children = new List<GameObject>();
        foreach(Transform child in btnParent.transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        float offsetY = -10;
        float margin = 4;
        float left = 0 + (btnParent.GetComponent<RectTransform>().rect.width / 2);
        float height = commandDisplayPrefab.GetComponent<RectTransform>().rect.height;

        float curHeight = offsetY - (height / 2);

        int programLength = botProgram.Length();
        for(int x = 0; x < programLength; ++x) {
            VCPUProgramCommand curCommand = botProgram.Get(x);

            GameObject newButton = Instantiate(commandDisplayPrefab);
            newButton.transform.SetParent(btnParent.transform);
            newButton.transform.localPosition = new Vector2(left, curHeight);
            curHeight -= newButton.GetComponent<RectTransform>().rect.height + margin;

            Transform textCommand = newButton.GetComponent<RectTransform>().Find("TextCommandName");
            //textCommand.GetComponent<Text>().text = "#" + (x + 1) + ": " + curCommand;
            Debug.Log(textCommand);


        }
    }

    public void CommandRemove() {

    }

    public void CommandMoveUp() {

    }

    public void CommandMoveDown() {

    }

    public void CommandConfigValues() {

    }

    public void OnCommandAddClick(Button button) {
        string command = button.GetComponent<ProgrammerAddCommand>().commandName;

        VCPUProgram botProgram = target.GetProgram();
        botProgram.Add((VCPUProgramCommand)commands[command].Clone());

        ShowProgram();
    }

    public void Refresh() {
        if(commands == null) {
            commands = new Dictionary<string, VCPUProgramCommand>();
        }

        commands.Clear();

        commands["REGISTER_STORE"] = new VCPUProgramCommandRegisterStore();
        commands["REGISTER_LOAD"] = new VCPUProgramCommandRegisterLoad();
        commands["END"] = new VCPUProgramCommandEnd();

        Debug.Log(this);

        Bot curBot = GetComponent<Bot>();
        if(curBot != null) {

            VCPUProgram botProgram = target.GetProgram();

            //POPULATE!
            target = curBot;
            
            GameObject ui = GameObject.Find("EditorUI");

            //GameObject commandList = GameObject.FindGameObjectWithTag("UICommandsAvailable");       //SCROLLVIEW
            GameObject btnParent = GameObject.Find("CommandListContent");

            var children = new List<GameObject>();
            foreach(Transform child in btnParent.transform) children.Add(child.gameObject);
            children.ForEach(child => Destroy(child));

            
            float offsetY = -10;
            float margin = 3;
            float left = 0 + (btnParent.GetComponent<RectTransform>().rect.width / 2);
            float height = prefab.GetComponent<RectTransform>().rect.height;

            float curHeight = offsetY - (height / 2);

            foreach(var pair in commands) {
                GameObject newButton = Instantiate(prefab);
                newButton.transform.SetParent(btnParent.transform);
                newButton.transform.localPosition = new Vector2(left, curHeight);
                newButton.GetComponent<Button>().GetComponentInChildren<Text>().text = pair.Value.GetName();
                curHeight -= newButton.GetComponent<RectTransform>().rect.height + margin;

                newButton.GetComponent<ProgrammerAddCommand>().commandName = pair.Key;



                newButton.GetComponent<Button>().onClick.AddListener(delegate () { OnCommandAddClick(newButton.GetComponent<Button>()); });

            }


        } else {
            Debug.Log("Can only program bots!");
        }


    }

}
