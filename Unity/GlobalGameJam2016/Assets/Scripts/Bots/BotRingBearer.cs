﻿using UnityEngine;
using System.Collections;

public class BotRingBearer : Bot {
    VCPU _cpu;

    float _partialCycles;
    bool _engines;
    float _curSpeed;

    bool _hasRing;

    public float thrust;
    public float weight;
    public float maxSpeed;
    public uint cyclesPerSecond;

    protected float _time = 0.0f;

    public override void ResetCPU() {
        _cpu.Reset();
    }

    // Use this for initialization
    public override void Start () {

        /*
        commandsAvailable[0] = VCPUProgramCommandRegister();
        commandsAvailable[0] = VCPUProgramCommandRegisterStore();
        commandsAvailable[0] = VCPUProgramCommandRegisterStore();
        */
        /*
        commandsAvailable[0] = VCPUProgramCommandRegisterStore();
        commandsAvailable[0] = VCPUProgramCommandRegisterStore();
        commandsAvailable[0] = VCPUProgramCommandRegisterStore();
        commandsAvailable[0] = VCPUProgramCommandRegisterStore();
        */

        _hasRing = true;
        _engines = false;
        _curSpeed = 0;

        _cpu = new VCPU();
        _partialCycles = 0;

        /*
        VCPUProgram blankProgram = new VCPUProgram();

        blankProgram.Add(new VCPUProgramCommandRegisterLoad(0,1,false));
        blankProgram.Add(new VCPUProgramCommandRegisterLoad(1,0, false));
        blankProgram.Add(new VCPUProgramCommandRegisterLoad(2,1, false));
        blankProgram.Add(new VCPUProgramCommandEnd());
        */
        //blankProgram.add
        //blankProgram.Add(new VCPUProgramCommand((uint)VCPU.INSTRUCTIONS.REGISTER_LOAD,new uint[] { 5 }));
        //blankProgram.Add(new VCPUProgramCommand((uint)VCPU.INSTRUCTIONS.JUMP,new uint[] { 3 }));
        //blankProgram.Add(new VCPUProgramCommand((uint)VCPU.INSTRUCTIONS.STOP));
        //blankProgram.Add(new VCPUProgramCommand((uint)VCPU.INSTRUCTIONS.INC, new uint[] { 0 }));
        //blankProgram.Add(new VCPUProgramCommand((uint)VCPU.INSTRUCTIONS.JUMP,new uint[] { 1 }));

        //_cpu.Load(blankProgram);
        //_cpu.Boot();

        Debug.Log("Starting up CPU...");
	}

    // Update is called once per frame
    public override void Update() {
        if(_cpu.IsHalted()) {
            Debug.Log("CPU has halted!");
        }
        //CALCULATE THE NUMBER OF CYCLES
        float cycles = Time.deltaTime * cyclesPerSecond;

        uint extraCyclesThisFrame = (uint)Mathf.FloorToInt(_partialCycles);
        uint realCycles = (uint)Mathf.FloorToInt(cycles) + extraCyclesThisFrame;

        _partialCycles += cycles - realCycles;

        //DO CPU STUFF
        _cpu.Step(realCycles);


        Debug.Log(_cpu.GetRegister(0));

        bool lastEngines = _engines;
        bool changedEngineState = false;
        //READ REGISTERS
        if(_cpu.GetRegister(0) >= 1) {
            _engines = true;
        } else {
            _engines = false;
        }

        if(lastEngines != _engines) {
            changedEngineState = true;
        }

        

        //FIGURE OUT WHICH WAY THE BOT IS MOVING IN
        TileMovement tileMove = GetComponent<TileMovement>();
        if(tileMove != null) {
            Vector2 curPos = tileMove.GetTargetPos();

            if(tileMove.AtTargetPos() || changedEngineState) {
                if(_engines) {
                    if(_cpu.GetRegister(1) >= 1) { curPos.y += 1; }          //DOWN
                    if(_cpu.GetRegister(2) >= 1) { curPos.x -= 1; }         //LEFT
                    if(_cpu.GetRegister(3) >= 1) { curPos.y -= 1; }         //UP   
                    if(_cpu.GetRegister(4) >= 1) { curPos.x += 1; }         //RIGHT

                }
            }

            tileMove.SetTargetPos((int)curPos.x, (int)curPos.y, 0.5f);
        }
        _time += Time.deltaTime;
	}
}
