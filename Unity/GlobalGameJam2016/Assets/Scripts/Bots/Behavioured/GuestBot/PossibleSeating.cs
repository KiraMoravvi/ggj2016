﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington
using System.Collections.Generic;

public class PossibleSeating : MonoBehaviour
{
    protected static PossibleSeating _instance;

    [SerializeField]
    protected int[] _yPos;

    [SerializeField]
    protected int[] _xStart;

    [SerializeField]
    protected int[] _xEnd;

    [SerializeField]
    protected List<PositionStruct> _occupied = new List<PositionStruct>();

    public Vector2 GetRandomPosition()
    {
        int x = 0;
        int y = 0;

        int maxLoops = 2560;
        int loops = 0;

        do
        {
            y = _yPos[Random.Range(0,_yPos.Length)];
            int index = Random.Range(0,_xStart.Length);
            x = Random.Range(_xStart[index],_xEnd[index]);
            loops++;
        } while (!PositionFree(x,y) && loops <= maxLoops);

        _occupied.Add(new PositionStruct(x, y));

        return new Vector2(x, y);
    }

    protected bool PositionFree(int x, int y)
    {
        bool free = true;
        foreach (PositionStruct occup in _occupied)
        {
            if (occup.x == x)
            {
                if (occup.y == y)
                {
                    free = false;
                    break;
                }
            }
        }
        return free;
    }

    public static PossibleSeating Instance()
    {
        if (_instance != null)
        {
            return _instance;
        }
        else
        {
            _instance = FindObjectOfType<PossibleSeating>();
            return _instance;
        }
    }
}