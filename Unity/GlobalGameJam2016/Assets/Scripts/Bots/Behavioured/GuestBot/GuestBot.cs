﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

public class GuestBot : MonoBehaviour
{
    [SerializeField]
    protected TileMovement _movement;

    protected int targetX = 0;
    protected int targetY = 0;

    public bool IsAllowed = true;

    public void Awake()
    {
        IsAllowed = (Random.Range(0, 100) > 50);
        StartCoroutine(MoveToQueue());
    }

    public IEnumerator MoveToQueue()
    {
        yield return new WaitForSeconds(0.1f);

        MoveToQueueY();
        StartCoroutine(StartMoving());
    }

    protected void MoveToQueueY()
    {
        _movement.SetTargetY(29, 0.1f);
    }

    public IEnumerator StartMoving()
    {
        while (_movement.IsMoving())
        {
            yield return new WaitForSeconds(0.1f);
        }
        if (IsAllowed)
        {
            MoveToAisleY();
            StartCoroutine(MoveToSeatX());
        }
        else
        {
            MoveToOutgoingX();
            StartCoroutine(MoveToOutgoing());
        }
    }

    protected void MoveToOutgoingX()
    {
        _movement.SetTargetX(31, 0.1f);
    }

    public IEnumerator MoveToOutgoing()
    {
        while (_movement.IsMoving())
        {
            yield return new WaitForSeconds(0.1f);
        }
        DoMoveOutY();
    }

    protected void DoMoveOutY()
    {
        _movement.SetTargetY(32, 0.1f);
        Invoke("TurnOff",1.0f);
    }

    void TurnOff()
    {
        this.gameObject.SetActive(false);
    }

    protected void MoveToAisleY()
    {
        MapGenerator.Instance()._tileScripts[targetY][targetX].SetWalkable(true);
        Vector2 randPos = PossibleSeating.Instance().GetRandomPosition();
        targetX = (int)randPos.x;
        targetY = (int)randPos.y - 1;
        _movement.SetTargetY(targetY, 0.1f);
    }

    public IEnumerator MoveToSeatX()
    {
        while (_movement.IsMoving())
        {
            yield return new WaitForSeconds(0.1f);
        }
        DoMoveX();
    }

    protected void DoMoveX()
    {
        _movement.SetTargetX(targetX, 0.1f);
        StartCoroutine(StartSitting());
    }

    public IEnumerator StartSitting()
    {
        while (_movement.IsMoving())
        {
            yield return new WaitForSeconds(0.1f);
        }
        DoSit();
    }

    protected void DoSit()
    {
        targetY += 1;
        _movement.SetTargetY(targetY, 0.1f);
        MapGenerator.Instance()._tileScripts[targetY][targetX].SetWalkable(false);
    }
}