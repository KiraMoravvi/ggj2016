﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

[System.Serializable]
public class PositionStruct : MonoBehaviour
{
    public int x;
    public int y;

    public PositionStruct(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}