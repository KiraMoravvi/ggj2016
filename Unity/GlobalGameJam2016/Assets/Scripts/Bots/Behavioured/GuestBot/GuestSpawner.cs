﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

public class GuestSpawner : MonoBehaviour
{
	[SerializeField]
    protected GameObject _guestPrefab;

    [SerializeField]
    protected int _max;

    protected int _count = 0;

    [SerializeField]
    protected float _spawnDelay = 0.1f;

    void Awake()
    {
        StartCoroutine(Spawn());
    }

    protected IEnumerator Spawn()
    {
        yield return new WaitForSeconds(_spawnDelay);

        _count++;

        GameObject spawned = GameObject.Instantiate(_guestPrefab);

        spawned.transform.parent = this.transform;

        if (_count <= _max)
        {
            StartCoroutine(Spawn());
        }
    }
}