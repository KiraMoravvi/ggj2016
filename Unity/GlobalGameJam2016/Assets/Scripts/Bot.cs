﻿using System.Collections;
using UnityEngine;

public class Bot : MonoBehaviour {
    public string[] intRegisterDescriptions = new string[16];
    public string[] floatRegisterDescriptions = new string[16];

    public VCPUProgramCommand[] availableCommands;

    protected VCPUProgram _program;

    public Bot() {

    }

    public void SetProgram(VCPUProgram program) {
        _program = program;
    }

    public VCPUProgram GetProgram() {
        if(_program == null) {
            _program = new VCPUProgram();
        }
        return _program;
    }

    public virtual void ResetCPU() {

    }

    public virtual void Start() {

    }

    public virtual void Update() {

    }
}
