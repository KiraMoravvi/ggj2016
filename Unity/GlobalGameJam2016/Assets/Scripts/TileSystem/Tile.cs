﻿using UnityEngine;
using System.Collections;
using System;

// Author: Dean Farrington

public class Tile : MonoBehaviour
{
	protected int _spriteId;
	
	protected Vector3 _pos;
	
	protected GameObject _gameObject;
	
	protected SpriteRenderer _spriteRenderer;
	
	protected Transform _parent;
	
	protected bool _walkable;
	
	public void SetUp(int spriteId, Transform parent)
	{
		_spriteId = spriteId;
		_parent = parent;
		
		Initialize();
	}
	
	public void SetPosition(Vector3 pos)
	{
		_pos = pos;
		_gameObject.transform.localPosition = pos;
	}
	
	protected virtual void Initialize()
	{
		int id = (int)(Mathf.Floor(_spriteId / 10f));
		int index = (int)(_spriteId % 10f);
		
		_gameObject = new GameObject();
		_spriteRenderer = _gameObject.AddComponent<SpriteRenderer>();
		_spriteRenderer.sprite = SpriteData.Instance().GetSprite(id ,index);
		_gameObject.transform.parent = _parent;
		
		string x = ((int)_pos.x).ToString();
		string y = ((int)_pos.y).ToString();
		_gameObject.transform.name = "Tile - " + _spriteId + " {" + x + "," + y + "}";
		
		_walkable = SpriteData.Instance().GetWalkable(id);
	}
	
	public void SetWalkable(bool isWalkable)
	{
		_walkable = isWalkable;
	}

    public bool IsWalkable()
    {
        return _walkable;
    }
}