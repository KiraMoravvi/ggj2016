﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

public class MapParser
{
	protected static MapParser _instance;
	
	protected int[][] _intIds;
	
	public int[][] GetMap(TextAsset mapFile)
	{
		string[] lines = new string[0];
		if(mapFile != null)
		{
			lines = (mapFile.text.Split('\n'));
		}
		
		_intIds = new int[lines.Length][];
		
		for (int i = 0; i < lines.Length; i++)
		{
			string[] ids;
			ids = lines[i].Split(',');
			
			_intIds[i] = new int[ids.Length];
			for (int j = 0; j < ids.Length; j++)
			{
				_intIds[i][j] = int.Parse(ids[j]);
			}
		}
		
		return _intIds;
	}
	
	public static MapParser Instance()
	{
		if (_instance != null)
		{
			return _instance;
		}
		else
		{
			_instance = new MapParser();
			return _instance;
		}
	}
}