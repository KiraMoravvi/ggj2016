﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

public class MapGenerator : MonoBehaviour
{
    protected static MapGenerator _instance;

    protected int _width;
    protected int _height;

    [SerializeField]
    protected float _spacing;

    public Tile[][] _tileScripts;

    [SerializeField]
    protected Transform _parent;

    [SerializeField]
    protected SpriteRenderer _exampleSpriteRenderer;

    [SerializeField]
    protected TextAsset _mapFile;

    protected int[][] _mapIds;

    protected virtual void Awake()
    {
        _mapIds = MapParser.Instance().GetMap(_mapFile);
        _width = _mapIds[0].Length;
        _height = _mapIds.Length;

        Vector3 size = _exampleSpriteRenderer.bounds.size;

        _spacing = size.x;

        _tileScripts = new Tile[_height][];

        for (int i = 0; i < _height; i++)
        {
            _tileScripts[i] = new Tile[_width];
            for (int j = 0; j < _width; j++)
            {
                //int id = (int)(Mathf.Floor(_mapIds[i][j]/ 10f));
                //int index = (int)(_mapIds[i][j] % 10f);

                int id = (int)_mapIds[i][j];
                id = (int)(Mathf.Floor(_mapIds[i][j]/ 10f));

                _tileScripts[i][j] = Instantiate(SpriteData.Instance().GetTileType(id));

                id = (int)_mapIds[i][j];
                _tileScripts[i][j].SetUp(id, _parent);
                _tileScripts[i][j].SetPosition(GetPosition(j, i));
                Destroy(_tileScripts[i][j].gameObject);
            }
        }

        CameraController.Instance().PositionCamera(_spacing, _width, _height);
    }

    public Vector3 GetPosition(int x, int y)
    {
        return new Vector3(x * _spacing, 0 - (y * _spacing), 0f);
    }

    public static MapGenerator Instance()
    {
        if (_instance != null)
        {
            return _instance;
        }
        else
        {
            _instance = FindObjectOfType<MapGenerator>();
            return _instance;
        }
    }
}