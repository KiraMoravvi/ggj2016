﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

[System.Serializable]
public class SpriteInfo
{
    public int _id;
    public Sprite[] _sprites;
    public Tile _tileType;
    public int _index;
    public bool _walkable;
}