﻿using UnityEngine;
using System.Collections;

public class TestTile : Tile {

	protected override void Initialize()
    {
        _gameObject = new GameObject();
        _spriteRenderer = _gameObject.AddComponent<SpriteRenderer>();
        _spriteRenderer.sprite = SpriteData.Instance().GetSprite(_spriteId);
        _gameObject.transform.parent = _parent;
    }
}
