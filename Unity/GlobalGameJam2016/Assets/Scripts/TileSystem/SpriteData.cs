﻿using UnityEngine;
using System.Collections;

// Author: Dean Farrington

public class SpriteData : MonoBehaviour
{
	protected static SpriteData _instance;
	
	[SerializeField]
	protected Sprite[] _sprites;
	
	[SerializeField]
	protected Tile[] _tileTypes;
	
	[SerializeField]
	protected bool[] _walkable;
	
	[SerializeField]
	protected SpriteInfo[] _spriteInfos;
	
	public static SpriteData Instance()
	{
		if (_instance != null)
		{
			return _instance;
		}
		else
		{
			_instance = FindObjectOfType<SpriteData>();
			return _instance;
		}
	}
	
	public Sprite GetSprite(int id, int index = 0)
	{
		if (id < 0 || id >= _spriteInfos.Length)
			return null;
		
		//return _sprites[id];
		return _spriteInfos[id]._sprites[index];
	}
	
	public Tile GetTileType(int id)
	{
		if (id < 0 || id >= _spriteInfos.Length)
			return null;
		
		//return _tileTypes[id];
		return _spriteInfos[id]._tileType;
	}
	
	public bool GetWalkable(int id)
	{
		if (id < 0 || id >= _walkable.Length)
			return false;
		
		//return _walkable[id];
		return _spriteInfos[id]._walkable;
	}
}